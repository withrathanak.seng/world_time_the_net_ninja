import 'package:flutter/material.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {

  Map locationData = {};

  @override
  Widget build(BuildContext context) {

    // this actually execute before returning anything so setState() is not required
    this.locationData = this.locationData.isNotEmpty ? this.locationData : ModalRoute.of(context).settings.arguments;
    String bgImage = this.locationData['isDayTime'] ? 'assets/images/day.png' : 'assets/images/night.png';

    return Scaffold(
      backgroundColor: Colors.black26,
      // not using AppBar -> SafeArea: move your body to the screen display
      body: SafeArea(
        child: Container(
          child: Padding(
            padding: const EdgeInsets.fromLTRB(0.0, 120.0, 0.0, 0.0),
            child: Column(
              children: [
                CircleAvatar(
                  backgroundImage: AssetImage('$bgImage'),
                  backgroundColor: Colors.transparent,
                  radius: 40.0,
                ),
                SizedBox(height: 100.0),
                FlatButton.icon(
                  onPressed: () async {
                   dynamic result = await Navigator.pushNamed(context, '/locations');
                   setState(() {
                     this.locationData = {
                       'location': result['location'],
                       'flag': result['flag'],
                       'time': result['time'],
                       'isDayTime': result['isDayTime'],
                     };
                   });
                  },
                  icon: Icon(Icons.edit_location),
                  label: Text('Change location'),
                  color: Colors.white,
                ),
                SizedBox(height: 20.0),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      '${this.locationData['location']}',
                      style: TextStyle(
                        fontSize: 28.0,
                        letterSpacing: 2.0,
                        color: Colors.white,
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 20.0),
                Text(
                  '${this.locationData['time']}',
                  style: TextStyle(
                    fontSize: 66.0,
                    color: Colors.white,
                  ),
                ),
              ],
            ),
          ),
        )
      ),
    );
  }
}
