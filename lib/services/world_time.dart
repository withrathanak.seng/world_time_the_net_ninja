import 'package:http/http.dart';
import 'dart:convert';
import 'package:intl/intl.dart';

class WorldTime {

  String location; // location name for the UI
  String time; // time in this location
  String flag; // url to flat icon
  String url; // url to the location api end-point
  bool isDayTime; // true or false if daytime or not

  WorldTime({this.location, this.flag, this.url});
  WorldTime.empty();

  Future<void> getTime() async {

    try {

      Response response = await get('http://worldtimeapi.org/api/timezone/${this.url}');
      Map data = jsonDecode(response.body);

      // get properties from the response
      String datetime = data['datetime'];
      String offset = data['utc_offset'];

      // create datetime obj
      DateTime now = DateTime.parse(datetime);
      now = now.add(Duration(hours: int.parse(offset.substring(1, 3))));

      // set the time properties
      this.isDayTime = now.hour > 6 && now.hour < 20 ? true : false;
      this.time = DateFormat.jm().format(now);

    } catch (error) {
      print('Caught error: $error');
      this.time = 'could not get datetime data';
    }

  }
}