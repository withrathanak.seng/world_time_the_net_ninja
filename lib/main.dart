import 'package:flutter/material.dart';
import 'package:world_time_the_net_ninja/pages/choose_location.dart';
import 'package:world_time_the_net_ninja/pages/home.dart';
import 'package:world_time_the_net_ninja/pages/loading.dart';

void main() {
  runApp(MaterialApp(
    initialRoute: '/',
    routes: {
      '/': (context) => Loading(),
      '/home': (context) => Home(),
      '/locations': (context) => ChooseLocation(),
    },
  ));
}